[Setup]
AppName=Quattuor
AppVersion=1.1.0
AppVerName=Quattuor 1.1.0
AppPublisher=Roma Aeterna
WizardStyle=modern
LicenseFile=LICENSE.txt
DefaultDirName={autopf}\Quattuor
DisableProgramGroupPage=Yes
UninstallDisplayIcon={app}\quattuor.exe
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64
Compression=lzma2
SolidCompression=yes
OutputDir=installer
OutputBaseFilename=QuattuorSetup

[Files]
Source: "dist\*"; DestDir: "{app}"; Flags: recursesubdirs

[Icons]
Name: "{autoprograms}\Quattuor"; Filename: "{app}\quattuor.exe"

[Languages]
Name: "en"; MessagesFile: "compiler:Default.isl"
Name: "de"; MessagesFile: "compiler:Languages\German.isl"
