#!/usr/bin/env python3
#
# This file is part of Quattuor.
#
# Copyright (C) 2020-2021 - Thomas Dähnrich <develop@tdaehnrich.de>
#
# Quattuor is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Quattuor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quattuor. If not, see <http://www.gnu.org/licenses/>.

import configparser
import gettext
import gi
import locale
import os
gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')
from gi.repository import Gdk
from gi.repository import Gtk


# set application name and version

APP = 'quattuor'
APP_ID = 'de.RomaAeterna.Quattuor'
VERSION = '1.1.0'


# set application paths

HOME_DIR = os.path.expanduser('~')
if os.name == 'posix':
    CONFIG_DIR = os.path.join(HOME_DIR, '.config', APP)
else:
    CONFIG_DIR = os.path.join(os.getenv('APPDATA'), APP)
CONFIG_FILE = os.path.join(CONFIG_DIR, 'config.ini')
TEAMS_FILE = os.path.join(CONFIG_DIR, 'teams.txt')
VOC_LISTS_FILE = os.path.join(CONFIG_DIR, 'voc_lists.txt')

# application installed by Meson
if os.path.exists('@PKGDATADIR@') and os.path.exists('@LOCALEDIR@'):
    DATA_DIR = '@PKGDATADIR@'
    LOCALE_DIR = '@LOCALEDIR@'
# application uninstalled or MS Windows environment
else:
    ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))
    if not os.path.exists(os.path.join(ROOT_DIR, 'data')):
        ROOT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
    DATA_DIR = os.path.join(ROOT_DIR, 'data')
    LOCALE_DIR = os.path.join(ROOT_DIR, 'locale')

PIXMAPS_DIR = os.path.join(DATA_DIR, 'pixmaps')
SOUNDS_DIR = os.path.join(DATA_DIR, 'sounds')
UI_DIR = os.path.join(DATA_DIR, 'ui')


# main functions of settings

def get_settings():

    config = configparser.ConfigParser()
    if not config.read(CONFIG_FILE):
        os.makedirs(CONFIG_DIR, exist_ok=True)
        default_settings()
        save_config_file()
    else:
        read_config_file(config)


def default_settings():

    global dark_theme, larger_font, position_control, position_arrows, sound, timer_length
    global selection, default_folder, default_file

    dark_theme = False
    larger_font = False
    position_control = 0
    position_arrows = 0
    sound = True
    timer_length = 5
    selection = 'source'
    default_folder = HOME_DIR
    default_file = ''


def save_config_file():

    config = configparser.ConfigParser()
    config['General'] = {
        'dark_theme': dark_theme,
        'larger_font': larger_font,
        'position_control': position_control,
        'position_arrows': position_arrows,
        'sound': sound,
        'timer_length': timer_length}
    config['Vocabulary'] = {
        'selection': selection,
        'default_folder': default_folder,
        'default_file': default_file}
    try:
        with open(CONFIG_FILE, 'w', encoding='utf-8') as f:
            config.write(f)
    except PermissionError:
        message_text = _("Could not save configuration file: No write permissions.")
        error_dialog = Gtk.MessageDialog(self.winSettings, 0,
            Gtk.MessageType.ERROR, Gtk.ButtonsType.CLOSE, message_text)
        error_dialog.run()
        error_dialog.destroy()


def read_config_file(config):

    global dark_theme, larger_font, position_control, position_arrows, sound, timer_length
    global selection, default_folder, default_file

    modify_config = False

    try:
        dark_theme = config['General'].getboolean('dark_theme', fallback=False)
    except (KeyError, ValueError):
        dark_theme = False
        modify_config = True

    try:
        larger_font = config['General'].getboolean('larger_font', fallback=False)
    except (KeyError, ValueError):
        larger_font = False
        modify_config = True

    try:
        position_control = config['General'].getint('position_control', fallback=0)
    except (KeyError, ValueError):
        position_control = 0
        modify_config = True

    try:
        position_arrows = config['General'].getint('position_arrows', fallback=0)
    except (KeyError, ValueError):
        position_arrows = 0
        modify_config = True

    try:
        sound = config['General'].getboolean('sound', fallback=True)
    except (KeyError, ValueError):
        sound = True
        modify_config = True

    try:
        timer_length = config['General'].getint('timer_length', fallback=5)
    except (KeyError, ValueError):
        timer_length = 5
        modify_config = True

    try:
        selection = config['Vocabulary'].get('selection', fallback='source')
    except (KeyError, ValueError):
        selection = 'source'
        modify_config = True

    try:
        default_folder = config['Vocabulary'].get('default_folder', fallback=HOME_DIR)
    except (KeyError, ValueError):
        default_folder = HOME_DIR
        modify_config = True
    finally:
        if not os.path.exists(default_folder):
            default_folder = HOME_DIR
            modify_config = True

    try:
        default_file = config['Vocabulary'].get('default_file', fallback='')
    except (KeyError, ValueError):
        default_file = ''
        modify_config = True
    finally:
        if default_file and not os.path.exists(default_file):
            default_file = ''
            modify_config = True

    if modify_config:
        save_config_file()


def setup_language():

    global _

    # setup for Linux
    if os.name == 'posix':
        locale.bindtextdomain(APP, LOCALE_DIR)
        locale.textdomain(APP)

    # setup for MS Windows
    else:
        import ctypes
        libintl = ctypes.cdll.LoadLibrary('libintl-8')
        # call encode() to prevent encoding errors with non-ASCII characters
        libintl.bindtextdomain(APP.encode('utf-8'), LOCALE_DIR.encode('utf-8'))
        libintl.bind_textdomain_codeset(APP.encode('utf-8'), 'UTF-8'.encode('utf-8'))
        os.environ['LANG'] = locale.getdefaultlocale()[0]

    _ = gettext.gettext
    gettext.bindtextdomain(APP, LOCALE_DIR)
    gettext.textdomain(APP)
    locale.setlocale(locale.LC_ALL, '')

    return _


def get_scale_factor(winGame):

    if os.name == 'posix':
        if not Gtk.check_version(3,22,0):
            display = Gdk.Display.get_default()
            monitor = display.get_monitor_at_window(winGame.get_window())
            scale_factor = monitor.get_scale_factor()
        else:
            screen = Gdk.Screen.get_default()
            monitor = screen.get_monitor_at_window(winGame.get_window())
            scale_factor = screen.get_monitor_scale_factor(monitor)
    else:
        import winreg
        try:
            key = winreg.OpenKey(winreg.HKEY_CURRENT_USER,
            "Control Panel\Desktop\WindowMetrics", 0, winreg.KEY_READ)
            dpi = winreg.QueryValueEx(key, "AppliedDPI")[0]
            winreg.CloseKey(key)
            scale_factor = dpi / 96
        except Exception:
            scale_factor = 1

    if larger_font:
        scale_factor = scale_factor * 0.85

    return scale_factor


def load_css_provider(scale_factor):

    with open(os.path.join(UI_DIR, 'quattuor.css'), encoding='utf-8') as css_file:
        css_data = css_file.read()

    if scale_factor != 1:
        for percent in (200, 150, 125):
            new_value = round(percent/scale_factor)
            if new_value < 100:
                new_value = 100
            css_data = css_data.replace(str(percent), str(new_value))
    if os.name == 'posix':
        background_image_path = os.path.join(PIXMAPS_DIR, 'background.jpg')
        css_data = css_data.replace('../pixmaps/background.jpg', background_image_path)
    else:
        css_data = css_data.replace('../pixmaps/background.jpg', 'data/pixmaps/background.jpg')

    css_provider = Gtk.CssProvider()
    css_provider.load_from_data(css_data.encode())
    Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(), css_provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)


def add_keyboard_accelerators(self):

    accel_group = Gtk.AccelGroup()
    accel_group.connect(Gdk.keyval_from_name('Escape'), 0, 0, self.on_winSettings_delete_event)
    self.winSettings.add_accel_group(accel_group)


def set_filter_for_file_button(filebutton):

    filter_csv = Gtk.FileFilter()
    filter_csv.set_name('CSV')
    filter_csv.add_mime_type('text/csv')
    filter_csv.add_pattern('*.[Cc][Ss][Vv]')
    filebutton.add_filter(filter_csv)

    filter_txt = Gtk.FileFilter()
    filter_txt.set_name('Text')
    filter_txt.add_mime_type('text/plain')
    filter_txt.add_pattern('*.[Tt][Xx][Tt]')
    filebutton.add_filter(filter_txt)


# initialize settings window and manage widgets

class Settings(Gtk.Window):

    def __init__(self, transient_window):

        self.builder = Gtk.Builder()
        self.builder.add_from_file(os.path.join(UI_DIR, 'settings.ui'))
        self.builder.connect_signals(self)

        for obj in self.builder.get_objects():
            if issubclass(type(obj), Gtk.Buildable):
                name = Gtk.Buildable.get_name(obj)
                setattr(self, name, obj)

        add_keyboard_accelerators(self)
        set_filter_for_file_button(self.filbtnVocabularyFile)

        self.winSettings.set_transient_for(transient_window)
        self.winSettings.show()


    def on_winSettings_show(self, widget):

        from game import team_names

        # General
        self.swtDarkTheme.set_active(dark_theme)
        self.swtLargerFont.set_active(larger_font)
        if position_control == 0:
            self.btnGameControlLeft.set_active(True)
        else:
            self.btnGameControlRight.set_active(True)
        if position_arrows == 0:
            self.btnArrowTop.set_active(True)
        else:
            self.btnArrowBottom.set_active(True)
        self.swtSound.set_active(sound)
        self.spnbtnTimerLength.set_value(timer_length)

        # Vocabulary
        if selection == "source":
            self.btnSelectionSource.set_active(True)
        if selection == "target":
            self.btnSelectionTarget.set_active(True)
        if selection == "both":
            self.btnSelectionBoth.set_active(True)
        self.filbtnVocabularyFolder.set_filename(default_folder)
        if default_file:
            self.filbtnVocabularyFile.set_filename(default_file)

        # Teams & Players
        if not team_names:
            self.gridSettingsTeams.hide()
        else:
            self.gridSettingsTeams.show()
            self.listTeams.clear()
            team_names.sort()
            for team_name in team_names:
                self.listTeams.append([team_name])


    def on_swtDarkTheme_activated(self, widget, gparam):

        global dark_theme

        dark_theme = widget.get_active()
        Gtk.Settings.get_default().set_property("gtk-application-prefer-dark-theme", dark_theme)


    def on_swtLargerFont_activated(self, widget, gparam):

        from game import winGame
        global larger_font

        larger_font = widget.get_active()

        scale_factor = get_scale_factor(winGame)
        load_css_provider(scale_factor)


    def on_btnGameControl_toggled(self, widget):

        from game import boxGame, boxGameControl
        global position_control

        if self.btnGameControlLeft.get_active():
            position_control = 0
            boxGame.child_set_property(boxGameControl, "pack-type", Gtk.PackType.START)
        else:
            position_control = 1
            boxGame.child_set_property(boxGameControl, "pack-type", Gtk.PackType.END)


    def on_btnArrow_toggled(self, widget):

        from game import gridGameGrid, position_buttons
        global position_arrows

        if self.btnArrowTop.get_active():
            position_arrows = 0
            for i in range(len(position_buttons)):
                gridGameGrid.remove(position_buttons[i])
                gridGameGrid.attach(position_buttons[i], i, 0, 1, 1)
                position_buttons[i].set_label("▼")
        else:
            position_arrows = 1
            for i in range(len(position_buttons)):
                gridGameGrid.remove(position_buttons[i])
                gridGameGrid.attach(position_buttons[i], i, 7, 1, 1)
                position_buttons[i].set_label("▲")


    def on_swtSound_activated(self, widget, gparam):

        global sound

        sound = widget.get_active()


    def on_spnbtnTimerLength_value_changed(self, widget):

        global timer_length

        timer_length = widget.get_value_as_int()


    def on_btnSelection_toggled(self, widget):

        global selection

        if self.btnSelectionSource.get_active():
            selection = "source"
        if self.btnSelectionTarget.get_active():
            selection = "target"
        if self.btnSelectionBoth.get_active():
            selection = "both"


    def on_filbtnVocabularyFolder_file_set(self, widget):

        global default_folder

        default_folder = widget.get_filename()


    def on_btnVocabularyFolderClear_clicked(self, widget):

        global default_folder

        default_folder = HOME_DIR
        self.filbtnVocabularyFolder.set_filename(HOME_DIR)


    def on_filbtnVocabularyFile_file_set(self, widget):

        global default_file

        default_file = widget.get_filename()
        if not default_file:
            default_file = ''


    def on_btnVocabularyFileClear_clicked(self, widget):

        global default_file

        default_file = ''
        self.filbtnVocabularyFile.unselect_all()


    def on_treeTeams_row_activated(self, widget, row, column):

        model, treeiter = widget.get_selection().get_selected()

        if column.get_title():
            # start editing on single mouse click
            widget.set_cursor(row, column, True)
        else:
            # remove team when delete symbol was clicked
            from game import team_names
            team_names.remove(model[treeiter][0])
            self.listTeams.remove(treeiter)


    def on_cellTeamName_edited(self, renderer, path, text):

        from game import team_names

        for i in range(len(team_names)):
            if team_names[i] == self.listTeams[path][0]:
                team_names[i] = text
                break

        self.listTeams[path][0] = text


    def on_cellTeamName_editing_started(self, renderer, editable, path):

        editable.set_max_length(20)


    def on_winSettings_delete_event(self, widget, event, *args):

        from game import save_team_names

        save_config_file()
        save_team_names()

        self.winSettings.hide()
        return True
