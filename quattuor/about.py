#!/usr/bin/env python3
#
# This file is part of Quattuor.
#
# Copyright (C) 2020-2021 - Thomas Dähnrich <develop@tdaehnrich.de>
#
# Quattuor is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Quattuor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quattuor. If not, see <http://www.gnu.org/licenses/>.

import gi
import os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from settings import UI_DIR, VERSION


# initialize about window and manage widgets

class About(Gtk.AboutDialog):

    def __init__(self, transient_window):

        self.builder = Gtk.Builder()
        self.builder.add_from_file(os.path.join(UI_DIR, 'about.ui'))
        self.builder.connect_signals(self)

        for obj in self.builder.get_objects():
            if issubclass(type(obj), Gtk.Buildable):
                name = Gtk.Buildable.get_name(obj)
                setattr(self, name, obj)

        self.abtdlg.set_version(VERSION)
        self.abtdlg.set_transient_for(transient_window)
        self.abtdlg.show()


    def on_abtdlg_response(self, widget, response_id):

        if response_id == Gtk.ResponseType.DELETE_EVENT:
            widget.hide()
