# -*- mode: python ; coding: utf-8 -*-

import os
import sys

bin_dir = os.path.split(sys.executable)[0]

bin_files = [
    (os.path.join(bin_dir, 'gspawn-win64-helper.exe'), '.'),
    (os.path.join(bin_dir, 'gspawn-win64-helper-console.exe'), '.')
]

data_files = [
    ('LICENSE.txt', '.'),
    ('NEWS.txt', '.'),
    ('README.rst', '.'),    
    ('data/pixmaps', 'data/pixmaps'),
    ('data/sounds', 'data/sounds'),
    ('data/ui', 'data/ui'),
    ('locale/de/LC_MESSAGES/quattuor.mo', 'locale/de/LC_MESSAGES'),
    ('locale/en_GB/LC_MESSAGES/quattuor.mo', 'locale/en_GB/LC_MESSAGES')
]

exclude_modules = [
    'lib2to3',
    'tcl',
    'tk',
    '_tkinter',
    'tkinter',
    'Tkinter'
]

path_extensions = [
    os.path.join(os.getcwd(), 'quattuor')
    ]

a = Analysis(
    ['quattuor/quattuor.py'],
    pathex = path_extensions,
    binaries = bin_files,
    datas = data_files,
    hiddenimports = [],
    hookspath = [],
    runtime_hooks = [],
    excludes = exclude_modules,
    win_no_prefer_redirects = False,
    win_private_assemblies = False,
    cipher = None,
    noarchive = False
)

pyz = PYZ(
    a.pure,
    a.zipped_data,
    cipher = None
)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries = True,
    name = 'quattuor',
    debug = False,
    bootloader_ignore_signals = False,
    strip = False,
    upx = True,
    console = False,
    icon = 'data/icons/quattuor.ico'
)

coll = COLLECT(
    exe,
    a.binaries,
    a.zipfiles,
    a.datas,
    strip = False,
    upx = True,
    upx_exclude = [],
    name = 'quattuor'
)
